# jeestudio

#### 介绍

        Conflux BPM是一款支持区块链的开源Java低代码开发平台，
    采用微服务架构（前后端分离），同时支持PC端和移动端。是一个集
    成了表单引擎、工作流、区块链智能合约和区块链NoSQL数据库的开
    源企业级JAVA快速开发微服务框架（BPM）。采用目前流行的多种
    Web2.0技术，包括Spring Boot, MyBatis, Shiro, Redis, 
    Jquery ,BootStrap, Activiti，JWT等；底层同时支持多种数据
    库例如MySQL, Oracle, Sqlserver，国产达梦，智能合约NoSQL
    等数据库；同时使用新型Web3.0技术：稳妥、高速、低费用、国产自
    主知识产权的Conflux树图区块链，将最好的体验献给所有用户。

#### 安装教程

    1. https://shimo.im/docs/9HxTDxRgXhdWPq8H

#### 项目说明
    
    1. jeestudio-admin 后台管理
    2. jeestudio-cache 缓存库(服务)
    3. jeestudio-datasource 数据源库(服务)
    4. jeestudio-form 表单生成库(服务)
    5. jeestudio-gateway 网关接口
    6. jeestudio-queen 队列库(服务)
    7. jeestudio-utils 工具类库
    8. jeestudio-workflow 工作流库(服务)
    9. jeestudio-common 公用基础库（jar）

#### 联系我们

    学习交流QQ群：855297742